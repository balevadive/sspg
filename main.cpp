// SPDX-License-Identifier: GPL-3.0-only

#include<iostream>
#include<random>
#include<algorithm>
#include<getopt.h>
#include "clip/clip.h"
#include "sspg.h"

std::string generatePassword(int passLength, int minSpecialChars, int minDigits, int minLower, int minUpper);
int convertAndValidateArgument(char* arg);

// need to make this constexpr in future versions of c++
static const std::string usage = R"(SSPG.

    Usage:
      sspg [--length <LENGTH>] [-s <SYMS>] [-d <DIGS>]
      sspg (-h | --help)
      sspg --version

    Options:
      -h --help         Show this screen.
      -v --version      Show version.
      -n --length       Length of password [default: 16].
      -s --symbols      Minimum number of symbols [default: 1].
      -d --digits       Minimum number of digits [default: 1].
      -l --lowercase    Minimum number of lowercase characters [default: 1].
      -u --uppercase    Minimum number of uppercase characters [default: 1].
)";

int main(int argc, char* argv[]) {
    
    static struct option long_options[] =
    {
	/* These options don’t set a flag.
	   We distinguish them by their indices. */
	{"version",   no_argument,       0, 'v'},
	{"help",      no_argument,       0, 'h'},
	{"length",    required_argument, 0, 'n'},
	{"symbols",   required_argument, 0, 's'},
	{"digits",    required_argument, 0, 'd'},
	{"lowercase", required_argument, 0, 'l'},
	{"uppercase", required_argument, 0, 'u'},	
	{0, 0, 0, 0}
    };
    /* getopt_long stores the option index here. */
    int option_index = 0;
    
    std::string password;

    // Default values
    int passLength = 16;
    int minSpecialChars = 1;
    int minDigits = 1;
    int minLower = 1;
    int minUpper = 1;

    // Parse command line arguments
    int iarg;
    while ((iarg = getopt_long(argc, argv, ":vhn:s:d:l:u:", long_options, &option_index)) != EOF)
    {
        switch (iarg)
        {
	    case 'v':
		std::cout <<  "SSPG version " << SSPG_VERSION_MAJOR << "." << SSPG_VERSION_MINOR << std::endl;
		return 0;
	    case 'h':
		std::cout << usage << std::endl;
		return 0;	    
            case 'n':
                passLength = convertAndValidateArgument(optarg);
                break;
            case 's':
                minSpecialChars = convertAndValidateArgument(optarg);
                break;
            case 'd':
                minDigits = convertAndValidateArgument(optarg);
                break;
            case 'l':
                minLower = convertAndValidateArgument(optarg);
                break;
            case 'u':
                minUpper = convertAndValidateArgument(optarg);
                break;
            case ':':
                std::cout << "Option " << (char)optopt << " requires an operand" << std::endl;
                return 1;
            case '?':
                std::cout << "Unrecognized option: " << (char)optopt << std::endl;
		std::cout << "\nTo see usage, run:\n\t" << argv[0] <<" --help" << std::endl;
                return 1;
        }
    }

    // validating arguments
    if (passLength < minSpecialChars + minDigits + minLower + minUpper)
    {
        std::cout << "Password length must be at least the sum of the minimum number of special characters, digits, lowercase letters, and uppercase letters" << std::endl;
        return 1;
    }

    // Generate password
    password = generatePassword(passLength, minSpecialChars, minDigits, minLower, minUpper);

    clip::set_text(password); // Copy password to clipboard
    std::cout << "Password copied to clipboard" << std::endl;
    return 0;
}

int convertAndValidateArgument(char* arg) {
    int convertedArg;

    // Convert argument to int and check if it is a number
    try{
        int convertedArg = std::stoi(arg);
        if (convertedArg < 0) {
            std::cout << "Argument must be a positive integer" << std::endl;
            exit(1);
        }

        return convertedArg;
    }
    catch(std::invalid_argument& e) {
        std::cout << "Invalid argument: '" << arg << "'. Argument must be a positive integer" << std::endl;
        exit(1);
    }
}

std::string generatePassword(int passLength, int minSpecialChars, int minDigits, int minLower, int minUpper) {
    // all avalable chars
    const std::string SPECIALCHARS = "!@#$%^&*_+-?";
    const std::string DIGITS = "0123456789";
    const std::string LOWERCASE = "abcdefghijklmnopqrstuvwxyz";
    const std::string UPPERCASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    const std::string ALLCHARS = SPECIALCHARS + DIGITS + LOWERCASE + UPPERCASE;

    std::string password = "";


    // random number generator
    std::random_device rd;
    std::mt19937 mt(rd());

    // distribution functions
    std::uniform_int_distribution<int> distSpecialChars(0, SPECIALCHARS.length() - 1);
    std::uniform_int_distribution<int> distNumbers(0, DIGITS.length() - 1);
    std::uniform_int_distribution<int> distLower(0, LOWERCASE.length() - 1);
    std::uniform_int_distribution<int> distUpper(0, UPPERCASE.length() - 1);
    std::uniform_int_distribution<int> distAllChars(0, ALLCHARS.length() - 1);

    // filling password with min special chars as per requirement
    for (int i = 0; i < minSpecialChars; i++) {
        password += SPECIALCHARS[distSpecialChars(mt)];
    }

    // filling password with min digits as per requirement
    for (int i = 0; i < minDigits; i++) {
        password += DIGITS[distNumbers(mt)];
    }

    // filling password with min lower case chars as per requirement
    for (int i = 0; i < minLower; i++) {
        password += LOWERCASE[distLower(mt)];
    }

    // filling password with min upper case chars as per requirement
    for (int i = 0; i < minUpper; i++) {
        password += UPPERCASE[distUpper(mt)];
    }

    // filling password with remaining random chars as per requirement
    for (int i = 0; i < passLength - minSpecialChars - minDigits - minLower - minUpper; i++) {
        password += ALLCHARS[distAllChars(mt)];
    }

    // shuffling password
    std::shuffle(password.begin(), password.end(), mt);

    return password;
}
